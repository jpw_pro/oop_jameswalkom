﻿using UnityEngine;
using System.Collections;

public class PlatformSpawn : MonoBehaviour 
{
	public GameObject platform;
	public Transform spawnpoint;
	public float spawnPointOffset;
	public RunScript script;
	// Use this for initialization
	void Start () 
	{
		script = GetComponent<RunScript>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter (Collider other)
	
		{
			Instantiate (platform, new Vector3 ((spawnpoint.position.x + spawnPointOffset), spawnpoint.position.y,spawnpoint.position.z), Quaternion.identity);
		}
		
}
