﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct Stats
{
	public int health;
	public string name;
	public float movementSpeed;
	public float JumpHeight;
	public KeyBinds controls;
}


public class Player : MonoBehaviour 

{
	public Rigidbody playaRigidbody;
	public float playaSpeed = 0.0f;
	public float playaJump = 0.0f;
	public Stats mystats;
	public int score;
	// Use this for initialization
	void Start () {
		playaRigidbody = GetComponent<Rigidbody> ();
	}



	
	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate () 
	{
		checkKeyPress (); 
	}
	public void checkKeyPress() 
	{
		if (Input.GetKeyDown (mystats.controls.forwardkey))
			playaRigidbody.AddForce (Vector3.forward * playaSpeed, ForceMode.Impulse);
		if (Input.GetKeyDown (mystats.controls.leftkey)) 
			playaRigidbody.AddForce (Vector3.left * playaSpeed, ForceMode.Impulse);	
		
		if (Input.GetKeyDown (mystats.controls.backkey)) 
			playaRigidbody.AddForce (Vector3.back * playaSpeed, ForceMode.Impulse);	
		
		if (Input.GetKeyDown (mystats.controls.rightkey)) 
			playaRigidbody.AddForce (Vector3.right * playaSpeed, ForceMode.Impulse);	
		
		if (Input.GetKeyDown (mystats.controls.jumpkey))
			playaRigidbody.AddForce (Vector3.up * playaJump, ForceMode.Impulse);	
		
	}

}
