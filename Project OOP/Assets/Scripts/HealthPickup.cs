﻿using UnityEngine;
using System.Collections;

public class HealthPickup : MonoBehaviour 
{
	public int recoverAmount;
	
	private void destroypickup()
	{
	Destroy (gameObject);
	}
	private void addHealth(Player targetPlayer)
	{
		targetPlayer.mystats.health += recoverAmount;
	}
	private void OnTriggerEnter (Collider col)
	{
		Player triggeredPlayer = col.gameObject.GetComponent<Player>();
		if (triggeredPlayer != null)
		{
		addHealth (triggeredPlayer);
		destroypickup ();
		}
	}
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
