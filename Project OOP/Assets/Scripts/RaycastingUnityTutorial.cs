﻿using UnityEngine;
using System.Collections;

public class RaycastingUnityTutorial : MonoBehaviour 
{
	
	public Rigidbody crate;
	public float parachuteEffectiveness;
	public float deploymentHeight;
	public bool deployed;
	
	// Use this for initialization
	void Start () 
	{
	crate = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		RaycastHit hit;
		Ray landingRay = new Ray(transform.position, Vector3.down);
		if (deployed)
		{
			if(Physics.Raycast(landingRay, out hit, deploymentHeight))
			{
				if (hit.collider.tag == "environment")
				{
					deployParachute();
				}
			}
		}
	}
	
	public void deployParachute()
	{
	deployed = true;
	crate.drag = parachuteEffectiveness;
	}
}
