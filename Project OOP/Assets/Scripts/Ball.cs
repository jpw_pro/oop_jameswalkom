﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour {

	public float bounce;
	public Rigidbody ballrb;
	
	
	void OnCollisionEnter(Collision other)
	{
	ballrb.AddForce (Vector3.up * bounce);
	}
	
	}

