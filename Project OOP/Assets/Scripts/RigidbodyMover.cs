﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public struct KeyBinds
{
	public KeyCode forwardkey;
	public KeyCode backkey;
	public KeyCode rightkey;
	public KeyCode leftkey;
	public KeyCode jumpkey;
}

public class RigidbodyMover : MonoBehaviour 
{
	public KeyBinds controls;
	public Rigidbody playaRigidbody;
	public float playaSpeed = 0.0f;
	public float playaJump = 0.0f;

	// Use this for initialization
	void Start () 
	{
		playaRigidbody = GetComponent<Rigidbody> ();

	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void FixedUpdate ()
	{		
		checkKeyPress ();
	}
//	An easy approach, but inputs but inupts still need to be set up in unity	
//	public void checkKeyPress() 
//	{
//		if (Input.GetKey (controls.forwardkey))
//			playaRigidbody.AddForce (Vector3.forward * playaSpeed, ForceMode.Impulse);
//		if (Input.GetKey (controls.leftkey)) 
//			playaRigidbody.AddForce (Vector3.left * playaSpeed, ForceMode.Impulse);	
//
//		if (Input.GetKey (controls.backkey)) 
//			playaRigidbody.AddForce (Vector3.back * playaSpeed, ForceMode.Impulse);	
//
//		if (Input.GetKey (controls.rightkey)) 
//			playaRigidbody.AddForce (Vector3.right * playaSpeed, ForceMode.Impulse);	
//
//		if (Input.GetKey (controls.jumpkey))
//			playaRigidbody.AddForce (Vector3.up * playaJump, ForceMode.Impulse);	
//
//	}
	//easier access. KeyCodes already assigned in code. No need to do it in unity. Saves time.
	public void checkKeyPress() 
	{
		if (Input.GetKeyDown (KeyCode.W))
			playaRigidbody.AddForce (Vector3.forward * playaSpeed, ForceMode.Impulse);
		if (Input.GetKeyDown (KeyCode.A)) 
			playaRigidbody.AddForce (Vector3.left * playaSpeed, ForceMode.Impulse);	
		
		if (Input.GetKeyDown (KeyCode.S)) 
			playaRigidbody.AddForce (Vector3.back * playaSpeed, ForceMode.Impulse);	
		
		if (Input.GetKeyDown (KeyCode.D)) 
			playaRigidbody.AddForce (Vector3.right * playaSpeed, ForceMode.Impulse);	
		
		if (Input.GetKeyDown (KeyCode.Space))
			playaRigidbody.AddForce (Vector3.up * playaJump, ForceMode.Impulse);	
		
	}


}