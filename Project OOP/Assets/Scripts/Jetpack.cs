﻿using UnityEngine;
using System.Collections;

public class Jetpack : MonoBehaviour 
{
	public KeyCode keyPress = KeyCode.Space;
	private Rigidbody rb;
	public float flySpeed = 20.0f;
	
	void Start()
	{
		rb = GetComponent<Rigidbody>();
	}
	
	void FixedUpdate()
	{
		UseJetPack ();
	}

	public void UseJetPack()
	{
		if(Input.GetKey(keyPress))
		{
			rb.useGravity = false;
			rb.AddForce(transform.up * flySpeed);
		}
		else
		{
			rb.useGravity = true;
		}
	}
}
