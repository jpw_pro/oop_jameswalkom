﻿using UnityEngine;
using System.Collections;

public class Key : MonoBehaviour {

	public GameObject Key1;
	public GameObject Door;
	public bool hasKey;
	
	// Use this for initialization
	void Start () 
	{
		noKey();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void noKey()
	{
		hasKey = false;
		Key1.SetActive(true);
	}
	
	public void pickupKey()
	{
		Key1.SetActive(false);
		hasKey = true;
	}
	public void lockedDoor()
	{
		hasKey = false;
		Door.SetActive(true);
	}
	
	public void unlockDoor()
	{
		hasKey = true; 
	}
	public void openDoor()
	{
		Door.SetActive(false);
	}
	
void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.tag == "Key")
		{
			pickupKey ();
		}
		if (other.gameObject.tag == "Door")
		{
			if (hasKey == true)
			{
				unlockDoor ();
				openDoor ();
			}
			if (hasKey == false)
			{
			lockedDoor ();
			}
			
		}


	}
}

