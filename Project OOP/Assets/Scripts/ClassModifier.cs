﻿using UnityEngine;
using System.Collections;

public class ClassModifier : MonoBehaviour 
	{
		public ClassPractice otherclass;
		// Use this for initialization
		void Start () 
		{
			otherclass.printdecimalNumber ();
			otherclass.printNumber();
			otherclass.printaWord ();
			otherclass.printanotherWord ();
			otherclass.printaBoolean();
		}
		
		// Update is called once per frame
		void Update () 
		{
			
		}
	}

