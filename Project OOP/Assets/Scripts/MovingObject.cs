﻿using UnityEngine;
using System.Collections;

public class MovingObject : MonoBehaviour 
{
	public Rigidbody Player;
	public float playerspeed;
	
//	public void moveForward()
//	{
//		Player.AddForce (Vector3.forward * playerspeed, ForceMode.Impulse);	
//		
//	}
//	public void moveBackward()
//	{
//		
//		Player.AddForce (Vector3.back * playerspeed, ForceMode.Impulse);
//	}
//	public void moveLeft()
//	{
//
//			Player.AddForce (Vector3.left * playerspeed, ForceMode.Impulse);
//	}
//	public void moveRight()
//	{
//			Player.AddForce (Vector3.right * playerspeed, ForceMode.Impulse);
//	}
	public void checkKeyPress()
	{
		if (Input.GetKey(KeyCode.W))
		{
//			moveForward();
			move (Vector3.forward, playerspeed);
		}
		if (Input.GetKey(KeyCode.S))
		{
//			moveBackward();
			move (Vector3.back, playerspeed);
		}
		if (Input.GetKey(KeyCode.A))
		{
//			moveLeft ();
			move (Vector3.left, playerspeed);
		}
		if (Input.GetKey(KeyCode.D))
		{
//			moveRight();
			move (Vector3.right, playerspeed);
		}
		if (Input.GetKeyDown (KeyCode.Space))
		{
			move (Vector3.up, playerspeed * 20);
		}
	}	
	private void move(Vector3 direction, float ForceAmount)
		{
			Player.AddForce(direction * ForceAmount);
		}
		
	
	
	
	// Use this for initialization
	void Start () 
	{
		Player = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () 
	{
	checkKeyPress();
	}
}
