﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public struct StructScope // created within file Scope
{//marks beginning of struct scope
	public string word;// created within the type/struct scope
	public int number;// created within the type/struct scope
	public bool boolean;// created within the type/struct scope
}//marks end of struct scope
public class ScopeTutorial : MonoBehaviour {
	
	private ScopeTutorial innerVariable;
	public StructScope structVariable; 
	public int classNumber; //is visible due to being inside the class
	public bool exists;
	public bool checkif;
	public string messageInABottle;
	public string message;
	private string secretMessage = "This is my secret";
	
	public string getSecretMessage()
	{
		return secretMessage;
	}
	
	public void printSecretMessage()
	{
		print (secretMessage);
	}
	public void changeSecretMessage(string newMessage)
	{
		if(newMessage.Length > 0 && newMessage.Length < 10)
		{
			secretMessage = newMessage;
		}
		else
		{
			Debug.LogWarning("You tried to change the secret message incorrectly");
		}
	
	 }
	// Use this for initialization
	void Start () 
	{
	//modified within the Start Scope, and accesses the struct's scope via the '.' operator
		structVariable.number = 1;
		structVariable.boolean = true;
		structVariable.word = "string";
		bool exists;
		exists = true;
		
		this.exists = exists;
		 scopeFunction ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		string messageInABottle;
		messageInABottle = "Message";
		
		this.messageInABottle = messageInABottle;
	}
	public void scopeFunction()
	{
		bool checkif;
		checkif = false;
		
		this.checkif = checkif;
		
			if(checkif = true)
			{
				string message;
				message = "Inside of if statement";
				this.message = message;
			}
	
	}
	
	
	
}
