﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct Item
{
public string name;
public string description;
public string useMessage;
}

public class PlayerInventory : MonoBehaviour 
{
public Item[] arrayItems;
public int currentIndex;

public void increaseIndex()
{
	currentIndex++;
}
 
public void decreaseIndex()
{
	currentIndex--;	
} 

public void useItem()
{
	print (arrayItems[currentIndex].useMessage);
}

public void checkKeys()
{
	if (Input.GetKeyDown (KeyCode.RightBracket))
	{
		if (currentIndex > 0)
		{
		increaseIndex();
		}
	}
	if (Input.GetKeyDown (KeyCode.LeftBracket))
	{
		if (currentIndex > 0)
		{
		decreaseIndex ();
		}
	}	
	if (Input.GetKeyDown (KeyCode.Space))
	{
	useItem();
	}
}

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		checkKeys ();
	}
}
