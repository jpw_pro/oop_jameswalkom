﻿using UnityEngine;
using System.Collections;

public class CoinCollector : MonoBehaviour {
	
	public int value;
	public Player ps;
	
	// Use this for initialization
	public void addAmount()
	{
		ps.score += value;
	}
	public void destroyCoin()
	{
	Destroy (gameObject);
	}
	
	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Player" )
		{
		addAmount();
		destroyCoin ();
		}
	}
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
