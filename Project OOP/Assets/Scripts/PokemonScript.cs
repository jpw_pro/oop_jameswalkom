﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct Pokemon
{
	public string name;
	public string gender;
	public int level;
	public string dexNumber;
	public int HP;
	public int attack;
	public int defense;
	public int spAttack;
	public int spDefence;
	public int speed;
	public string nature;
	public string ability;
	public string item;
	public string type;
	public string moveOne;
	public string moveTwo;
	public string moveThree;
	public string moveFour;



}



public class PokemonScript : MonoBehaviour {

	public Pokemon myPokemon;

	// Use this for initialization
	void Start () 
	{
		myFrogadier ();
	}
	
	// Update is called once per frame
	void Update () 
	{
	}
	public void myFrogadier ()

	{
		myPokemon.name = "Frogadier";
		myPokemon.gender = "male";
		myPokemon.level = 18;
		myPokemon.dexNumber = "008 Frogadier";
		myPokemon.HP = 51 / 51;
		myPokemon.attack = 33;
		myPokemon.defense = 29;
		myPokemon.spAttack = 37;
		myPokemon.spDefence = 27;
		myPokemon.speed = 46;
		myPokemon.nature = "LAX";
		myPokemon.ability = "Torrent";
		myPokemon.item = "None";
		myPokemon.type = "WATER";
		myPokemon.moveOne = "Water Pulse"; 
		myPokemon.moveTwo = "Lick";
		myPokemon.moveThree = "Bubble"; 
		myPokemon.moveFour = "Quick Attack";
	}



		
}
