﻿using UnityEngine;
using System.Collections;

public class ClassPractice : MonoBehaviour {
	
	public int number;
	public float decimalNumber;
	public bool aBoolean;
	public string aWord;
	public string anotherWord;	
	
	public void printNumber()
		
	{
		print (number);
	}
	
	public void printdecimalNumber()
		
	{
		print (decimalNumber);
	}
	
	public void printaBoolean()
		
	{
		print (aBoolean);
	}
	
	public void printaWord()
	{
		print (aWord);
	}
	
	public void printanotherWord()
	{
		print (anotherWord);
	}
	
	// Use this for initialization
	void Start () 
	{
		
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		
	}
}
