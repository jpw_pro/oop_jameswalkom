﻿using UnityEngine;
using System.Collections;

public class FunctionTutorial : MonoBehaviour 
{


	public string myName; 
	public int myAge;	
	public string mySex;
	public string aRandomSentence;
	public string myID;

	// Use this for initialization
	void Start () 

	{
		print (myName);
		print (myAge);
		print (mySex);
		print (aRandomSentence);
	}
	
	// Update is called once per frame
	void Update () 
	{
		checkKeyPress ();
	}
	public void checkKeyPress() 
	{
		if (Input.GetKey (KeyCode.W)) {
			print ("W was pressed");
			movePlayerForward ();
		}
		if (Input.GetKey (KeyCode.A)) {
			print ("A was pressed");
			movePlayerLeft ();
		}
		if (Input.GetKey (KeyCode.S)) {
			print ("S was pressed");
			movePlayerBack ();
		}
		if (Input.GetKey (KeyCode.D)) {
			print ("D was pressed");
			movePlayerRight ();
		}

		}
		public void movePlayerForward ()
	{
		print ("player is moving forward");
		transform.Translate (Vector3.forward * Time.deltaTime);
	}
		public void movePlayerBack()
	{
		print ("player is moving backward");
		transform.Translate (Vector3.back * Time.deltaTime);
	}
		public void movePlayerRight()
	{
		print ("player is moving right");
		transform.Translate (Vector3.right * Time.deltaTime);
	}
		public void movePlayerLeft ()
	{
		print ("player is moving Left");
		transform.Translate (Vector3.left * Time.deltaTime);
	}

}	
	