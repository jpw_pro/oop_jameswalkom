﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RunScript : MonoBehaviour 
{
	public Rigidbody player;
	public int speed;
	public int speedReturn;
	public int jump;
	public Vector3 checkpoint;
	public Transform playerTransform;
	public int score;
	public Text playerscore;
	public float currentTime;
	public float timeLimit;
	
	
	// Use this for initialization
	void Start () 
	{
		checkpoint = playerTransform.position;
		speedReturn = 500;
		speed = speedReturn;
		
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		playerScore();
		playerRun();
		playerJump();
	}
	public void playerRun()
	{
		player.AddForce(Vector3.right * speed * Time.deltaTime);
	
	}
	public void playerJump()
	{
		if(Input.GetKeyDown (KeyCode.Space))
		{
		player.AddForce(Vector3.up * jump, ForceMode.Impulse);
		}
	}
	public void playerScore()
	{
		playerscore.text = "Score: " + score.ToString();
		currentTime += Time.deltaTime;
		if (currentTime >= timeLimit)
		{
			score ++;
			currentTime = 0;
		}
	}
	
	
	void OnCollisionEnter (Collision other)
	{
		if (other.gameObject.tag == "Obstacle")
		{
			speed -=10;
			Destroy(other.gameObject);
		}
		if (other.gameObject.tag == "Pit")
		{
			Application.LoadLevel("ObstacleLvl1");
			speed = speedReturn;
			
		}
	}
	
}
