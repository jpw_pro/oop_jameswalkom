﻿using UnityEngine;
using System.Collections;

public class BallSpawner : MonoBehaviour 
{
	public float shootSpeed;
	public Rigidbody prefab;
	
	void Update () 
	{
		Shoot ();
	}
	
	private void Shoot()
	{
		if(Input.GetKeyDown (KeyCode.Return))
		{
			Rigidbody clone;
			clone = Instantiate (prefab, transform.position, transform.rotation) as Rigidbody;
			clone.velocity = transform.TransformDirection (Vector3.down * shootSpeed);
		}
	}
}
