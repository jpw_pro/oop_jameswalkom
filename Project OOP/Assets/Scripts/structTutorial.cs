﻿using UnityEngine;
using System.Collections;

// structs go here
[System.Serializable]
public struct Address
{
	public int houseNumber;
	public string streetName;
	public string state;
	public string city;
	public int postCode;
	public string country;
}





public class structTutorial : MonoBehaviour {

		public Address myAddress;
	 
	// Use this for initialization
	void Start () {
			setStartingAddress ();
	}

	// Update is called once per frame
	void Update (){
		if (Input.GetKeyDown (KeyCode.Space)) { 
			changeAddress();
		}
	}
	public void setStartingAddress()
	{
		myAddress.houseNumber = 1250;
		myAddress.streetName = "Pacific Highway";
		myAddress.state = "NSW";
		myAddress.city = "pymble";
		myAddress.postCode = 2073;
		myAddress.country = "Australia";
	}
	
	public void changeAddress ()
	{
		myAddress.houseNumber = 123;
		myAddress.streetName = "Fake Street";
		myAddress.state = "Springfield";
		myAddress.city = "Springfield";
		myAddress.postCode = 1000;
		myAddress.country = "Australia";
	}
}