﻿using UnityEngine;
using System.Collections;

public class ArrayRevision : MonoBehaviour 
{
	public int[] collectiveAges;
	public int arraySize;
	public int currentIndex;
	
	public void decreaseIndex()
	{
		
		currentIndex--;
		if (currentIndex < 0)
		{
		currentIndex =0;
		}
		 
	}
	
	public void increaseIndex()
	{
		currentIndex++;
		if (currentIndex > arraySize-1)//is currentIndex greater than the last element
		{
		currentIndex = arraySize - 1;
		}
	}
	
	
	public void setAge()
	{
		collectiveAges[currentIndex] = 20;
	}
	
	public void checkKeys()
	{
		if (Input.GetKeyDown (KeyCode.UpArrow))
		{
			increaseIndex ();
//			currentIndex++;
		}
		if (Input.GetKeyDown (KeyCode.DownArrow))
		{
			decreaseIndex ();
//			currentIndex -= 1;
			//currentIndex = currentIndex -1;
			//currentIndex--;
		}
		if (Input.GetKeyDown(KeyCode.Return))
		{
			setAge ();
		}
	}

	public void SetInitialValues()
	{
		for (int index = 0; index < collectiveAges.Length; index++)
		{
			collectiveAges[index] = -1;
			print (index);
		}
	}

	// Use this for initialization
	void Start () 
	{
	
	collectiveAges = new int[arraySize];
	SetInitialValues ();
	
	}

	// Update is called once per frame
	void Update () 
	{
		checkKeys ();
	}
}
