﻿using UnityEngine;
using System.Collections;

public class ArrayTutorial : MonoBehaviour 
{
	public string[] studentNames;
	public int arraySize;

	public void setNames()
	{ 
		studentNames [0] = "Adopted";
		studentNames [1] = "James";
		studentNames [2] = "Moe";
		studentNames [3] = "Luke";
		studentNames [4] = "Ellie";
		studentNames [5] = "Roy";

		for (int index = 0; index < studentNames.Length; index++) 
		{
		 studentNames[index] = "Student No." + index; 


		}
	}

	public void printNames ()
	{
		print (studentNames[0]); //print out the value of element 0
		print (studentNames [1]); //print out the value of element 1
		print (studentNames [2]); //print out the value of element 2
		print (studentNames [3]); //print out the value of element 3
		print (studentNames [4]); //print out the value of element 4
		print (studentNames [5]); //print out the value of element 5

//		for (int index = 0; index < studentNames.Length; index++) 
		{
			print(studentNames); 	
		}
	}
	public void testLoop ()
	{
		for(int index = 0; index < 10; index++)
		{
			print (index);
			//index --; //will cause infinite loop

		}
	}

	// Use this for initialization
	void Start () 
	{
		setNames();
		printNames ();

		testLoop ();

		studentNames = new string[arraySize];
	}



	// Update is called once per frame
	void Update () 
	{
	
	}
}
