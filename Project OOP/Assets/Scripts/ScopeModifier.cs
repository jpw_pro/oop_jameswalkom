﻿using UnityEngine;
using System.Collections;

public class ScopeModifier : MonoBehaviour {
	
	public ScopeTutorial otherClass;
	public string messageCopy;
	// Use this for initialization
	void Start () 
	{
		otherClass.scopeFunction();
		otherClass.classNumber = 4;
		otherClass.message = "mmk";
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown (KeyCode.Return))
		{
			otherClass.printSecretMessage();
		}
		if (Input.GetKeyDown (KeyCode.Backspace))
		{
			otherClass.changeSecretMessage("abc");
		}
		if (Input.GetKeyDown (KeyCode.Backslash))
		{
			messageCopy = otherClass.getSecretMessage();
		}
	}
}
