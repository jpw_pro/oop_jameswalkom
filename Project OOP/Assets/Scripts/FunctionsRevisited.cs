﻿using UnityEngine;
using System.Collections;

public class FunctionsRevisited : MonoBehaviour 
{
	public string myName;
	public int myAge;
	public string myGender;
	public float number1;
	public float number2;
	public float result;
	
	
	
	public void setName (string name)
	{
		myName = name;
	}
	
	public void setAge (int age)
	{
		myAge = age;
	}
	public void setGender(string gender)
	{
		myGender = gender;
	}
	public void setPersonalDetails(string Name, int Age, string Gender)
	{
		setName (Name);
		setAge (Age);
		setGender (Gender);
	}
	public void subtractNumbers(float num1, float num2)
	{
		result = num1 - num2;
		print (num1 -num2);
		
	}
	public void addNumbers(float num1, float num2)
	{
		result = num1 + num2;
		print (num1 + num2);
	}	
	public void multiplyNumbers(float num1, float num2)
	{
		result = num1 * num2;
		print (num1 * num2);
	}
	public void divideNumbers(float num1, float num2)
	{
		result = num1 / num2;
		print (num1 / num2);
	}	
	public void checkKeyPress()
	{
		if (Input.GetKeyDown (KeyCode.KeypadPlus))
		{
			addNumbers (number1, number2);
		}
		if (Input.GetKeyDown (KeyCode.KeypadMinus))
		{
			subtractNumbers (number1, number2);
		}
		if (Input.GetKeyDown (KeyCode.KeypadMultiply))
		{
			multiplyNumbers (number1, number2);
		}
		if (Input.GetKeyDown (KeyCode.KeypadDivide))
		{
			divideNumbers (number1, number2);
		}
	}
	
	// Use this for initialization
	void Start () 
	{
//	setName ("Jam");
//	setAge (31);
//	setGender ("Boy");
	setPersonalDetails ("James", 32, "Boy"); 
	
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		checkKeyPress ();
	}
}
