﻿using UnityEngine;
using System.Collections;

public class LoopUnityTutorial_DoWhileLoop : MonoBehaviour 
{
	void Start()
	{
		bool shouldContinue = false;
		
		do
		{
			print ("Hello World");
			
		}
		while(shouldContinue == true);
	}
}
